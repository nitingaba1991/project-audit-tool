
let appConfig = {};

appConfig.port = 4000;
appConfig.allowedCorsOrigin = "*";
appConfig.env = "dev";
appConfig.db = {
    uri: 'mongodb://127.0.0.1:27017/projectaudit'
  }
appConfig.apiVersion = '/api/v1';
appConfig.adminUsers = ['admin@gmail.com', 'admin1@gmail.com', 'admin2@gmail.com', 'admin3@gmail.com', 'admin4@gmail.com'];

module.exports = {
    port: appConfig.port,
    allowedCorsOrigin: appConfig.allowedCorsOrigin,
    environment: appConfig.env,
    db :appConfig.db,
    adminUsers: appConfig.adminUsers,
    apiVersion : appConfig.apiVersion
};