
let curdController = require('../controllers/curdController');


module.exports.setRouter = (app) => {
    
    app.get('/api/getData', curdController.getData);
    app.post('/api/addData', curdController.addData);
    
    // this is our update method
    // this method overwrites existing data in our database
    app.put('/api/updateData', curdController.updateData);
    
    // this is our delete method
    // this method removes existing data in our database
    app.delete('/api/deleteData', curdController.deleteData);
    
    // this is our create methid
    // this method adds new data in our database
    app.post('/api/addProjectData', curdController.addProjectData);
    app.post('/api/addQuestionData', curdController.addQuestionData);
}