
let userController = require('../controllers/userController');


module.exports.setRouter = (app) => {
    app.get('/user/info', userController.getUserInfo);
    app.get('/api/login', userController.login);
    app.post('/api/logout', userController.logout);
    app.post('/api/register', userController.register);
}