
let cartController = require('../controllers/cartController');


module.exports.setRouter = (app) => {
    app.post('/api/addToCart', cartController.addToCart);
}