
let dashboardController = require('../controllers/dashboardController');


module.exports.setRouter = (app) => {
    app.get('/api/getAllData', dashboardController.getAllEntityData);
    app.get('/api/:entity/view', dashboardController.getSingleEntityData);
}