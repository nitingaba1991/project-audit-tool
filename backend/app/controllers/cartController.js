var bcrypt = require('bcrypt');
var MongoClient = require('mongodb').MongoClient;
const appConfig = require('../../config/config');
var responseObj = function () {
    var Obj = {
        error: false,
        message: '',
        data: ''
    }
    return Obj
}

var connectDb = function (res, cb) {
    MongoClient.connect(appConfig.db.uri, function (err, db) {
        if (err) {
            var Obj = {
                error: true,
                message: err,
                data: ''
            }
            res.send('504', Obj);
            return;
        }
        DB = db;
        return cb(db);
    });
}

let addToCart = function (req, res, next) {
    var responseData = responseObj();
    // if (!req.session.user || !req.session.user.isLoggedIn || req.session.user.role !== 'admin') {
    //     responseData['message'] = 'Please Login First';
    //     res.send('500', responseData);
    // } else {
        connectDb(res, function (client) {
            var db = client.db();
            db.collection('cart').insert(req.body, function (err, data) {
                if (err) {
                    responseData['error'] = true;
                    responseData['message'] = 'Some Error in adding Product.';
                    res.send('504', responseData);
                } else {
                    responseData['data'] = data;
                    res.send(200, responseData);
                }
            })
        })
    // }
    
}

module.exports = {
    addToCart: addToCart
}