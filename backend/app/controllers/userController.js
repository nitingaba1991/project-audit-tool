var bcrypt = require('bcrypt');
var MongoClient = require('mongodb').MongoClient;
const appConfig = require('../../config/config');
var responseObj = function () {
    var Obj = {
        error: false,
        message: '',
        data: ''
    }
    return Obj
}

var connectDb = function (res, cb) {
    MongoClient.connect(appConfig.db.uri, function (err, db) {
        if (err) {
            var Obj = {
                error: true,
                message: err,
                data: ''
            }
            res.send('504', Obj);
            return;
        }
        DB = db;
        return cb(db);
    });
}

let logout = function (req, res, next) {
    console.log('before', req.session)
    req.session = null;
    var responseData = responseObj();
    res.clearCookie('connect.sid');
    res.send(200, 'Logout Successfully', responseData);
}

let getUserInfo = (req, res, next) => {
    res.type('application/javascript');
    
    if (req.session && req.session.user) {
        connectDb(res, function (client) {
            var db = client.db();
            db.collection('users').findOne({email: req.session.user.email.toLowerCase()}, function (err, data) {
                if (data && data.email) {
                    data.isLoggedIn = true;
                    delete data.password;
                    console.log(data)
                    req.session.user = data;
                    // var output = req.session.user;
                    // res.status(200).send(data);
                    var output = "session = " + JSON.stringify(data) + ";";
                    res.send(output);
                } else {
                    // var output = {isLoggedIn: false}
                    // res.status(200).send(output);
                    var output = "session = null";
                    res.send(output);                
                }
            })
        })
    } else {
        // var output = {isLoggedIn: false}
        // res.status(200).send(output)
        var output = "session = null";
        res.send(output);
    }
}


let login = function (req, res, next) {
    if (!req.query.email || !req.query.password) {
        res.send('500', 'Please enter the credential')
    }
    var responseData = responseObj();
    req.query['email'] = req.query['email'].toLowerCase();
    connectDb(res, function (client) {
        var db = client.db();
        db.collection('users').findOne({email: req.query.email}, function (err, data) {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = err;
                res.send('504', responseData);
            }
            if (data) {
                bcrypt.compare(req.query.password, data.password, function (err, rslt) {
                    if (rslt) {
                        delete data.password;
                        req.session.user = data;
                        console.log(data,'data is', req.session.user)
                        req.session.user.isLoggedIn = true;
                        responseData['data'] = data;
                        res.status(200).send(responseData);
                    } else {
                        responseData['error'] = true;
                        responseData['message'] = 'Check your credential';
                        res.send(404, responseData);
                    }
                });

            }
            else {
                responseData['error'] = true;
                responseData['message'] = 'Check your credential';
                res.send(404, responseData);
            }
        })
    })
}
let register = function (req, res, next) {
    var responseData = responseObj();
    connectDb(res, function (client) {
        var db = client.db();
        req.body['role'] = 'member';
        if(appConfig.adminUsers.indexOf(req.body.email) > -1 ) {
            req.body['role'] = 'admin';
        }
        req.body['created_on'] = new Date().getTime();
        req.body['email'] = req.body['email'].toLowerCase();
        db.collection('users').findOne({email: req.body.email}, function (err, data) {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = err;
                res.send('504', responseData);
            }
            if (data) {
                responseData['error'] = true;
                responseData['message'] = 'Email id already exist';
                res.send('401', responseData)
            }
            else {
                bcrypt.hash(req.body.password, 10, function (err, hash) {
                    req.body.password = hash;
                    db.collection('users').insert(req.body, function (err, data) {
                        if (err) {
                            responseData['error'] = true;
                            responseData['message'] = err;
                            res.send('504', responseData);
                        } else {
                            // req.session.user = req.body;
                            // delete req.body.password;
                            // req.session.user.isLoggedIn = true;
                            responseData['data'] = data;
                            res.send(200, responseData);
                        }
                    })
                });
            }
        })
    })
}
module.exports = {
    login: login,
    register: register,
    logout: logout,
    getUserInfo: getUserInfo
}