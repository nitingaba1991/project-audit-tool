// const Data = require('../../data');
var MongoClient = require('mongodb').MongoClient;
const appConfig = require('../../config/config');
var responseObj = function () {
    var Obj = {
        error: false,
        message: '',
        data: ''
    }
    return Obj
}
var connectDb = function (res, cb) {
    MongoClient.connect(appConfig.db.uri, function (err, db) {
        if (err) {
            var Obj = {
                error: true,
                message: err,
                data: ''
            }
            res.send('504', Obj);
            return;
        }
        DB = db;
        return cb(db);
    });
}
let getAllEntityData = (req, res, next) => {
    var query = {};
    if (req.query.q) {
        query = {
            item_name: {
                $regex: req.query.q,
                $options: 'i'
            }
        }
    }
    var responseData = responseObj();
    if (req.session && req.session.user && req.session.user.isLoggedIn && req.session.user.role == 'admin') {
        connectDb(res, function (client) {
            // console.log('query',query)
            var db = client.db();
            db.collection('products').count(function (err, productCount) {
                db.collection('users').count(function (err, usersCount) {
                    db.collection('orders').count(function (err, ordersCount) {
                        if (err) {
                            responseData['error'] = true;
                            responseData['message'] = err;
                            res.send('504', responseData);
                        } else {
                            console.log(productCount, usersCount);
                            responseData['data'] = {
                                productCount: productCount,
                                usersCount: usersCount,
                                ordersCount: ordersCount,
                            };
                            res.send('200', responseData);
                        }
                    })
                })
            })

        })
    } else {
        responseData['message'] = "Login required"
        res.send('403', responseData);
    }
}
let getSingleEntityData = (req, res, next) => {
    var query = {};
    if (req.query.q) {
        query = {
            item_name: {
                $regex: req.query.q,
                $options: 'i'
            }
        }
    }
    var responseData = responseObj();
    connectDb(res, function (client) {
        var db = client.db();
        db.collection(req.params.entity).find().toArray(function (err, data) {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = err;
                res.send('504', responseData);
            } else {
                responseData['data'] = data;
                res.send('200', responseData);
            }
        })

    })
}

module.exports = {
    getAllEntityData: getAllEntityData,
    getSingleEntityData: getSingleEntityData
}