// const Data = require('../../data');
var MongoClient = require('mongodb').MongoClient;
const appConfig = require('../../config/config');
const {ObjectId} = require('mongodb'); // or ObjectID 
var responseObj = function () {
    var Obj = {
        error: false,
        message: '',
        data: ''
    }
    return Obj
}
var connectDb = function (res, cb) {
    MongoClient.connect(appConfig.db.uri, function (err, db) {
        if (err) {
            var Obj = {
                error: true,
                message: err,
                data: ''
            }
            res.send('504', Obj);
            return;
        }
        DB = db;
        return cb(db);
    });
}
let getData = (req, res, next) => {
    var query = {};
    
    let collection = '';
    if (req.query.type) {
        collection = req.query.type
    } else {
        collection = 'products'
    }
    delete req.query.type;
    for (var key in req.query) {
        if (key === '_id') {
            query._id= ObjectId(req.query[key]);
        } else {
            query[key] = req.query[key];
        }
    }
    var responseData = responseObj();
    connectDb(res, function(client){
        var db = client.db();
        db.collection(collection).find(query).project({addedby: 0}).toArray(function (err, data) {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = err;
                res.send('504', responseData);
            } else {
                responseData['data'] = data;
                res.send('200', responseData);
            }
        })
       
    })
}
let updateData = (req, res) => {
    var query = {};
    var responseData = responseObj();
    if (req.body._id) {
        query = { _id: ObjectId(req.body._id) }
    }
    delete req.body._id;
    let collection = '';
    if (req.body.type) {
        collection = req.body.type
    } else {
        responseData.error = true;
        responseData.message = "Send type";
        res.send('200', responseData);
        return;
    }
    if(req.body.options) {
        req.body.options = req.body.options.split(',')
    }
    delete req.body.type;
    connectDb(res, function(client){
        var db = client.db();
        db.collection(collection).updateOne(query, {$set: req.body}, function (err, data) {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = err;
                res.send('504', responseData);
            } else {
                responseData['data'] = data;
                responseData['message'] = 'Updated Successfully';
                res.send('200', responseData);
            }
        })
       
    })
    
}

let deleteData = (req, res) => {
    const { id, type } = req.body;
    var responseData = responseObj();
    if(!req.body.id || !req.body.type) {
        responseData['error'] = true;
        responseData['message'] = 'Please enter Correct parameter';
        res.send('200',responseData);
    }
    connectDb(res, function(client){
        var db = client.db();
        db.collection(type).deleteOne({_id: ObjectId(id)}, (err, data) => {
            if (err) {
                responseData['error'] = true;
                responseData['message'] = 'Please enter Project name';
                res.send('500',responseData);
            } else {
                responseData['message'] = 'deleted Successfully';
                res.send('200',responseData);
            }
        });
    })
}

function checkDuplicacy(inputData, db, res) {
    var responseData = responseObj();
    db.collection('projects').findOne({projectId: inputData.pid}, function (err, data) {
        if (err) {
            responseData['error'] = true;
            responseData['message'] = 'Error in adding Project';
            res.send('500',responseData);
        }
        if (data) {
            inputData['pid'] = Math.floor(100000 + Math.random() * 900000);
            checkDuplicacy(inputData, db, res);
        }
        else {
            db.collection('projects').insertOne(inputData, function (err, data) {
                if (err) {
                    responseData['error'] = true;
                    responseData['message'] = err;
                    res.send('504', responseData);
                } else {
                    // console.log('ddsadadsaata',data.length,data)
                    responseData['data'] = data.result;
                    responseData['message'] = 'Added Successfully';
                    res.send('200', responseData);
                }
            })
        }
    })
}
let addProjectData = (req, res) => {
    var responseData = responseObj();
    var data = req.body;
    data['created_on'] = new Date().getTime();
    data['addedby'] = 'admin';
    data['project_name'] = data['project_name'].toLowerCase();
    data['updated_on'] = new Date().getTime();
    data['auditSubmited'] = false;
    data['pid'] = Math.floor(100000 + Math.random() * 900000);
    data['alias'] = data['project_name'].split(' ').join('');
    if(!data.project_name) {
        responseData['error'] = true;
        responseData['message'] = 'Please enter Project name';
    }
    if(responseData['error']) {
        res.send('200', responseData);
    } else {
        connectDb(res, function(client){
            var db = client.db();
            checkDuplicacy(data, db, res)
        })
    }
   
}

let addQuestionData = (req, res) => {
    var responseData = responseObj();
    var data = req.body;
    data['created_on'] = new Date().getTime();
    data['addedby'] = req.session.user.email;
    data['updated_on'] = new Date().getTime();
    if(!data.question_title) {
        responseData['error'] = true;
        responseData['message'] = 'Please enter question Title';
    }
    if(responseData['error']) {
        res.send('200', responseData);
    } else {
        if(data.options) {
            data.options = data.options.split(',');
        }
        connectDb(res, function(client){
            var db = client.db();
            db.collection('questions').insertOne(data, function (err, data) {
                if (err) {
                    responseData['error'] = true;
                    responseData['message'] = err;
                    res.send('504', responseData);
                } else {
                    responseData['error'] = false;
                    responseData['data'] = data.result;
                    responseData['message'] = 'Added Successfully';
                    res.send('200', responseData);
                }
            })
        })
    }
   
}
let addData = (req, res) => {
    var responseData = responseObj();
    var data = req.body;
    if(!data.project_id){
        responseData['error'] = true;
        responseData['message'] = 'Send project Id';
    }
    var project_id = req.body.project_id;
    data['created_on'] = new Date().getTime();
    data['reviewed_by'] = req.session.user.email;
    data['updated_on'] = new Date().getTime();
    var collection = 'audits';
    if(req.body.type) {
        collection = req.body.type;
    }
    if(responseData['error']) {
        res.send('200', responseData);
    } else {
        connectDb(res, function(client) {
            var db = client.db();
            db.collection(collection).insertOne(data, function (err, data) {
                if (err) {
                    responseData['error'] = true;
                    responseData['message'] = err;
                    res.send('504', responseData);
                } else { 
                    if(collection == 'audits') {
                        db.collection('projects').updateOne({_id: ObjectId(project_id)}, {$set: {auditSubmited: true}}, function (err, d) {
                        })
                    }
                    responseData['error'] = false;
                    responseData['data'] = data.result;
                    responseData['message'] = 'Added Successfully';
                    res.send('200', responseData);
                }
            })
        })
    }
   
}
module.exports = {
    getData: getData,
    addData: addData,
    updateData: updateData,
    deleteData: deleteData,
    addProjectData: addProjectData,
    addQuestionData: addQuestionData
}