//const mongoose = require('mongoose');
const express = require('express');
var cors = require('cors');
var path = require('path');

var session = require('express-session');
var cookieParser = require('cookie-parser');
var MongoStore = require('connect-mongo')(session);
const bodyParser = require('body-parser');
const logger = require('morgan');
const fs = require('fs');
const appConfig = require('./config/config');
const routesPath = './app/routes';

const app = express();
app.use(cookieParser());
// bodyParser, parses the request body to be a readable json format
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var store = new MongoStore(
  {
      url: appConfig.db.uri,
      ttl: 14 * 24 * 60 * 60 // = 14 days. Default
  });
app.use(session({
      secret: 'aersda@#$32sfas2362',
      saveUninitialized: false, // don't create session until something stored
      resave: true, //don't save session if unmodified
      cookie: {
          secure: false,
          maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week
      },
      store: store
  })
);
const corsOptions = {
  origin: 'http://localhost:4000', // This is where you put the URL of your frontend
  credentials: true,
};
app.use(cors(corsOptions));

// connects our back end code with the database
//mongoose.connect(appConfig.db.uri, { useNewUrlParser: true });
//let db = mongoose.connection;

//db.once('open', () => console.log('connected to the database'));

// checks if connection with the database is successful
//db.on('error', console.error.bind(console, 'MongoDB connection error:'));
process.on('uncaughtException', function (err) {
  console.log(err);
})
// Bootstrap route
fs.readdirSync(routesPath).forEach(function (file) {
  if (~file.indexOf('.js')) {
    let route = require(routesPath + '/' + file);
    route.setRouter(app);
  }
});
// (optional) only made for logging and
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '../build')));
app.use(function (req, res) {
    res.sendfile(path.join(__dirname, '../build/index.html'));
});


// launch our backend into a port
app.listen(appConfig.port, () => console.log(`LISTENING ON PORT ${appConfig.port}`));