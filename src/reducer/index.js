import { combineReducers } from 'redux'
import globalReducer from './operationReducer'
const combined = combineReducers({ globalReducer })
export default globalReducer;