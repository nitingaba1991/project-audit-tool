var configObj = {
    fieldType: [{
        name: 'Text',
        id: 'text'
    },
    {
        name: 'Textarea',
        id: 'textarea'
    },
    {
        name: 'Checkbox',
        id: 'checkbox'
    },
    {
        name: 'Radio',
        id: 'radio'
    }],
    questionCategoryTab: [{
        name: 'General',
        id: 'general'
    },
    {
        name: 'Development',
        id: 'development'
    },
    {
        name: 'Accessibility',
        id: 'accessibility'
    },
    {
        name: 'Performance',
        id: 'performance'
    }
]
}

export default configObj;