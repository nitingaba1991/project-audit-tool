import {toast } from 'react-toastify';
import axios from 'axios';

let showMessage = (error, msg) => {
    if (error) {
        toast.error(msg, {
            position: toast.POSITION.TOP_RIGHT
        });
    } else {
        toast.success(msg, {
            position: toast.POSITION.TOP_RIGHT
          });
    }
}
window.toastObj = {
    showMessage : showMessage 
}

let callAjax = (config) => {
    if(!config.method) {
        config.method = 'get'
    }
    axios(config,function(err, data){

    })
}
let obj = {
    showMessage : showMessage 
}
export default obj;