import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import {ToastContainer} from 'react-toastify';
import Loader from 'react-loader-spinner'
import Home from './components/homeComponent/home';
import NotFound from './components/notFoundComponent/notFound';
import Header from './components/header/header';
import SignUpComponent from './components/usersComponent/SignUpComponent';
import loginComponent from './components/usersComponent/loginComponent';
import listRecordComponent from './components/listRecordComponent/listRecordComponent';
import AddEditRecordComponent from './components/AddEditRecordComponent/AddEditRecordComponent';
import AuditComponent from './components/AuditComponent/AuditComponent';
import rootReducer  from './reducer';
import './middleware/middleware';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';
import './index.scss';
import 'react-toastify/dist/ReactToastify.css';
import { createStore } from 'redux'
import $ from 'jquery';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'
window.jQuery = $;
window.$ = $;
import 'bootstrap/dist/js/bootstrap.min.js';
let initialState = {
  user: {
    isLoggedIn: false
  },
}
if(window.session) {
  initialState['user'] = window.session;
}
let getLocalStorageCart = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')): {};
initialState['cart_id'] = getLocalStorageCart;
let store = createStore(rootReducer, initialState);
const Routing = (
    <Provider store = { store }>
  <Router>
      <div>
      <div
      style={{
        width: "100%",
        height: "100vh",
        display: "none",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Loader type="BallTriangle" color="#00BFFF" height={80} width={80} />
    </div>
      <Header></Header>
      <ToastContainer autoClose={2000}/>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/admin/view/:entity" component={listRecordComponent} />
        <Route path="/admin/:action/:entity" component={AddEditRecordComponent} />
        <Route path="/signup" component={SignUpComponent} />
        <Route path="/audit" component={AuditComponent} />
        <Route path="/login" component={loginComponent} />
        <Route component={NotFound} />
      </Switch>
      </div>

  </Router>
  </Provider>
)
ReactDOM.render(Routing, document.getElementById('root'));
