
// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import Modal from '../Modal/Modal';
import { connect } from 'react-redux';
class listRecordComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      selectedData: {},
      data: []
    }
    // this.goToProductPage = this.goToProductPage
  }
  componentDidMount() {
    if(!this.props.intialState.user.isLoggedIn) {
      this.props.history.push('/login');
      toastObj.showMessage(true, "Please Log In to view this page");
    }
    else if(this.props.intialState.user.isLoggedIn && this.props.intialState.user.role !== 'admin') {
      this.props.history.push('/');
      toastObj.showMessage(true, "Dont have access to view this page");
    }
    this.getDataFromDb();
  }
 
  deleteData = (item) => {
    this.setState({selectedData: item})
    $('.delete-modal').modal('show');
  }
  getDataFromDb = (query) => {
    let url = '/api/' + this.props.match.params.entity + '/view';
    axios.get(url)
      .then((res) => {this.setState({ data: res.data.data })});
  };

  editRecord = (item) => {
    this.props.history.push(`/admin/edit/questions?_id=${item._id}&type=questions`);
  }

  render() {
    let template = this.state.data.map(item => (
      <tr key={item._id}>
        <td  colSpan="2">{item.question_title}</td>
        <td>{item.category_type}</td>
        <td>{item.required === 'true' ? 'True' : 'False'}</td>
        <td><i className="fa mr-2 fa-edit" onClick={() => this.editRecord(item)} title="edit"></i> <i className="fa mr-2 fa-trash" onClick={() => this.deleteData(item)} title="delete"></i> </td> 
      </tr>
    ))
    
    return (
    <div className="container">
    <Modal getdata={this.getDataFromDb} id={this.state.selectedData._id} type="questions"/>
      <div className="row">
        <div className="col-12">
          <table className="mt-3 project table text-center">
            <thead>
              <tr>
              <th colSpan="2">Title</th>
              <th>Category</th>
              <th>Required</th>
              <th>Action</th>
              </tr>
            </thead>
            <tbody>
            {this.state.data && this.state.data.length ? template : <tr><td>No Questions Found. Please add Questions First.</td></tr>}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
    )


  }
}
const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps)(listRecordComponent);
