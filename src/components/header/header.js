
// /client/App.js
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {toast } from 'react-toastify';
import { connect } from 'react-redux';
import {updateLoginInfo} from '../usersComponent/loginComponent.actions';
import axios from 'axios';

class Header extends Component {
  constructor(props) {
    super();
    this.state = {
      value: ''
    }
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.logout = this.logout.bind(this);
  }
  handleSearchChange (event) {
    this.setState({value: event.target.value});
  }
  handleSearchSubmit(event) {
    event.preventDefault();
    let value = this.state.value;
    // this.props.history.push(`/products/?q=${value}`);
  }
  logout () {
    let url = '/api/logout';
    axios.post(url)
      .then((res) => {
        window.location.href= '/login';
    });
  }
 
  render() {
    let template = '';
    if(this.props.intialState.user.isLoggedIn) {
      template = <li className="list-inline-item">Hi {this.props.intialState.user.name},</li>;

    }
    let projectTemplate = (this.props.intialState.user.isLoggedIn && this.props.intialState.user.role === 'admin') ? 
    <ul className="list-inline mb-0 mt-2"> 
    <li className="list-inline-item mr-4"><Link to="/">Projects</Link></li>
    <li className="list-inline-item mr-4"><Link to="/admin/view/questions">Questions</Link></li>
    <li className="list-inline-item mr-4"><Link to="/admin/add/projects">Add Project</Link></li>
    <li className="list-inline-item mr-4"><Link to="/admin/add/questions">Add Question</Link></li>
     </ul>: '';
    return (
      <div className="header-container">
        <div className="header fixed-top">
        <div className="container">
          <div className="row">
          <div className="col-md-2 text-center">
                  <Link className="navbar-brand" to="/"><img className="logo" alt="logo" src="./logo.png"></img></Link>
                  <button className="navbar-toggler" type="button" data-toggle="collapse"
                      data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                      aria-label="Toggle navigation">
                      <span className="navbar-toggler-icon"></span>
                  </button>
              </div>
              <div className="col-md-8">
              {/* <form autoComplete="off" onSubmit={this.handleSearchSubmit} >
                <input className="search-field" id="search" onChange={this.handleSearchChange} type="text" placeholder="Search Projects"  />
              </form> */}
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                  </div>
                  <nav className="pt-3 navbar justify-content-center">
                    {projectTemplate}
                  </nav>
              </div>
              <div className="col-md-2">
                <ul className="list-inline pt-4 mb-0 text-right">
                {template}
                {!this.props.intialState.user.isLoggedIn ?
                <React.Fragment>               
                <li className="list-inline-item text-center">
                  <Link to="/login"><span className="login-link">Login</span></Link>
                </li>
                <li className="list-inline-item text-center">
                  <Link to="/signup"><span className="register-link">Register</span></Link>
                </li>
                </React.Fragment>
                :
                <Link onClick={this.logout}>Logout</Link>
                }
                </ul>
              </div>
          </div>
        </div>
    </div>
    </div>
    );
  }
}

const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
const mapDispatchToProps = dispatch => ({
  setLogin: (userData) => dispatch(updateLoginInfo(userData))
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
