// /client/App.js
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
class NotFound extends Component {
  
  render() {
    return (
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-6">
            <img src="./404.jpg" className="img-fluid" alt="404 Page" />
            <div className="text-center mt -3 mb-3"><Link to="/"><span className="login-link">Go to Home</span></Link></div>
          </div>
        </div>
      </div>
    );
  }
}

export default NotFound;