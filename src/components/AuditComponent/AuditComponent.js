
// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import configObj from '../../config'
import { connect } from 'react-redux';
import queryString from 'query-string'
class AuditComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      count: 0,
      questions: [],
      formData: { 
      }
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateState = this.updateState.bind(this);
  }
  
  componentDidMount() {
    if(!this.props.intialState.user.isLoggedIn) {
      this.props.history.push('/login');
      toastObj.showMessage(true, "Please Log In to view this page");
    } else {
      const values = queryString.parse(this.props.location.search);
      if(!values._id) {
       toastObj.showMessage(true, 'Please Select a project');
      this.props.history.push('/');
      } else {
        this.setState({formData:{project_id:values._id}});
        this.getDataFromDb();        
      }
    }
  }

  updateState (e) {
    var obj = this.state.formData;
    obj[e.target.name] = e.target.value;
    this.setState({formData: obj
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    let callAjax = true;
    $('textarea[required]').attr('required',true).each(function(i, requiredField){
      console.log('rrrrrrrrrrr',$(requiredField))
      if($(requiredField).val()=='')
      {
          toastObj.showMessage(true, 'Please check all fields in all tabs');
          callAjax = false;
          return false;
      }
  });
  if(callAjax) {
      var data = this.state.formData;
      var questionData = [];
      this.state.questions.forEach(function(item){
        if(data[item._id]) {
          questionData.push({title: item.question_title, answer: data[item._id]})
        }
      })
      let props = this.props;
      let finalData = {};
      finalData.project_id = this.state.formData.project_id;
      finalData.questionData = questionData;
      axios.post('/api/addData', finalData)
      .then(function (response) {
        toastObj.showMessage(response.data.error, 'Audit Submitted Successfully');
        props.history.push('/');
      })
      .catch(function (error) {
        toastObj.showMessage(true, 'Error in submitting!');
      });

    }
  }

  goToBack = (e) => {
    this.props.history.push('/');
  }

  getDataFromDb = () => {
    let url = `/api/getData?type=questions`;
    axios.get(url)
      .then((res) => {
        var categoryTab = [];
        this.setState({ questions: res.data.data });
      });
  };

  
  render() {
    let tabtemplate = configObj.questionCategoryTab.map((type, index) => (
        <li key={index} className={index == 0 ? 'active': ''}>
          <a  href={'#' + type.id} className={index == 0 ? 'active': ''} data-toggle="tab">{type.name}</a>
        </li>
    ))


    let template = configObj.questionCategoryTab.map((type, i) => {
      return <div className={ i == 0? "tab-pane active": "tab-pane"} key={type.id} id={type.id}>{this.state.questions.map((question, index) => (
        question.category_type === type.id ?
        <div className="form-group" key={question._id} >
          <label htmlFor="exampleFormControlTextarea1"><b>{question.question_title}{question.required == 'true' ? <span className="required"> *</span> : ''}</b></label>
          <textarea className="form-control" onChange={this.updateState} id={question._id} name={question._id} rows="3" required={question.required == 'true'? true : false}></textarea>
        </div>
        : ''
      ))}</div>;
    })

    return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-12 question-form">
          <h5>Audit Project</h5><br></br>
          <form autoComplete="off" noValidate onSubmit={this.handleSubmit}>
            <ul  className="nav nav-pills">
              {tabtemplate}
            </ul>
            <div className="tab-content clearfix">
              {template}
            </div>
            <button className="btn btn-primary mt-3" type="submit">Submit</button>
            <button className="btn btn-secondary mt-3 ml-3" onClick={() => this.goToBack()}>Cancel</button>
            </form>
        </div>
      </div>
    </div>
    
    )


  }
}
const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps)(AuditComponent);
