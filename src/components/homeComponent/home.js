// /client/App.js
import './home.css';
import React, { Component } from 'react';
import ListTable from '../listTable/listTable';
import { connect } from 'react-redux';
import axios from 'axios';
import {toast } from 'react-toastify';
import queryString from 'query-string'
class Home extends Component {
  constructor(props) {
    super();
    this.state = {
      data: []
    }
  }
  componentDidMount() {
    if(!this.props.intialState.user.isLoggedIn) {
      this.props.history.push('/login');
      toast.error("Please Log In to view this page", {
        position: toast.POSITION.TOP_RIGHT
      });
    } else {
      const values = queryString.parse(this.props.location.search)
      if(values.q) {
        this.getDataFromDb(values.q);
      } else {
        this.getDataFromDb();
      }
    }
    
  }
  getDataFromDb = (query) => {
    let url = '/api/projects/view';
    axios.get(url)
      .then((res) => {this.setState({ data: res.data.data })});
  };
  render() {
    
    return (
      <React.Fragment>
      <ListTable history={this.props.history} getDbData = {this.getDataFromDb} data={this.state.data}/>
      </React.Fragment>
    )

  }
}

const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps)(Home);
