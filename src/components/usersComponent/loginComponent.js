// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {toast } from 'react-toastify';
import { connect } from 'react-redux';
import {updateLoginInfo} from './loginComponent.actions';
class loginComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      error: false,
      emailError: '',
      passwordError: ''
    }
    this.callLogin = this.callLogin.bind(this);
  }
  componentDidMount = () => {
    if(this.props.intialState.user.isLoggedIn) {
      this.props.history.push('/');
      toastObj.showMessage(true, "Already LoggedIn");
    }
  }
  callLogin = (event) => {
    event.preventDefault();
    let data = {
      email: event.target.elements.email.value,
      password: event.target.elements.password.value
    }
    if(this.state.error) {
      this.setState({error: false, emailError: '', passwordError: ''});
    }
    if(data.email == '') {
      this.setState({error: true, emailError: 'Required Field'});
      return false;
    }
    if(data.email != '') {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var validEmail = regex.test(data.email);
      if(!validEmail) {
        this.setState({error: true, emailError: 'Please enter valid Email'});
        return false;
      }
    }

    if(data.password == '') {
      this.setState({error: true, passwordError: 'Required Field'})
      return false;
    }
    if(data.email != '' && data.password != '') {
      let url = '/api/login';
      axios.get(url, {params: data})
        .then((res) => {
          this.props.setLogin(res.data);
          this.props.history.push('/');
          toastObj.showMessage(res.data.error, "Login Successfully");
        })
        .catch(function (error) {
          toastObj.showMessage(true, "Error while login. Check credential!");
        });
    }
  }
  render() {
     return (
       <div className="container mt-3">
        <div className="row justify-content-center">
          <div className="col-md-4">
            <h3>Login</h3>
          <form onSubmit={this.callLogin} noValidate>
              <div className="form-group">
                <label>Email<span className="required">*</span></label>
                <input type="email" className="form-control" id="email" name="email"/>
                {this.state.error ?<span className="error">{this.state.emailError}</span>: ''}
              </div>
              <div className="form-group">
                <label>Password<span className="required">*</span></label>
                <input type="password" className="form-control" id="password" name="password"/>
                {this.state.error ?<span className="error">{this.state.passwordError}</span>: ''}
              </div>
              <button className="btn-primary btn" type="submit">Submit</button>
              <div className="mt-3">
                <Link to="/signup"><span className="register-link">Click here to Register?</span></Link>
              </div>
            </form>     
          </div>
        </div>
       </div>
      );
  }
}
const mapDispatchToProps = dispatch => ({
    setLogin: (userData) => dispatch(updateLoginInfo(userData))
});
const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(loginComponent);
