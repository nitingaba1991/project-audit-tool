// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import {toast } from 'react-toastify';
import { connect } from 'react-redux';
class SignUpComponent extends Component {
  constructor(props) {
    super();
    this.state = {
      error: false
    }
    this.callRegister = this.callRegister.bind(this);
  }
  componentDidMount = () => {
    if(this.props.intialState.user.isLoggedIn) {
      this.props.history.push('/');
      toastObj.showMessage(true, "Already LoggedIn");
    }
  }
  callRegister = (event) => {
      event.preventDefault();
      
      let data = {
        email: event.target.elements.registerEmail.value,
        name: event.target.elements.name.value,
        password: event.target.elements.registerPassword.value,
      }
      if(this.state.error) {
        this.setState({error: false, nameError: '',emailError: '',passwordError: '',confirmPwdError: '',});
      }
      if(data.name == '') {
        this.setState({error: true, nameError: 'Required Field'})
        return false;
      }
      if(data.email == '') {
        this.setState({error: true, emailError: 'Required Field'});
        return false;
      }
      if(data.email != '') {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var validEmail = regex.test(data.email);
        if(!validEmail) {
          this.setState({error: true, emailError: 'Please enter valid Email'});
          return false;
        }
      }
  
      if(data.password == '') {
        this.setState({error: true, passwordError: 'Required Field'})
        return false;
      }
      
      if(event.target.elements.registerConfirmPassword.value == '' || event.target.elements.registerConfirmPassword.value != data.password) {
        this.setState({error: true, confirmPwdError: 'Please enter same password'})
        return false;
      }
      let props = this.props;
      let url = '/api/register';
      axios.post(url, data)
      .then(function (response) {
        toast.success("Register Successfully. Please Login", {
          position: toast.POSITION.TOP_RIGHT
        });
        props.history.push('/login');
      })
      .catch(function (error) {
        toast.success("Error in Register. Please try again", {
          position: toast.POSITION.TOP_RIGHT
        });
      });
  }
  render() {
     return (
       <div className="container mt-3">
        <div className="row justify-content-center">
        <div className="col-md-4">
            <h3>Register</h3>
          <form autoComplete="off" onSubmit={this.callRegister} noValidate>
              <div className="form-group">
                <label>Name<span className="required">*</span></label>
                <input type="Name" className="form-control" id="name" name="name"/>
                {this.state.error ? <span className="error">{this.state.nameError}</span>: ''}
              </div>
              <div className="form-group">
                <label>Email<span className="required">*</span></label>
                <input type="email" className="form-control" id="registerEmail" name="registerEmail"/>
                {this.state.error ?<span className="error">{this.state.emailError}</span>: ''}
              </div>
              <div className="form-group">
                <label>Password<span className="required">*</span></label>
                <input type="password" className="form-control" id="registerPassword" name="registerPassword"/>
                {this.state.error ?<span className="error">{this.state.passwordError}</span>: ''}

              </div>
              <div className="form-group">
                <label>Confirm Password<span className="required">*</span></label>
                <input type="password" className="form-control" id="registerConfirmPassword" name="registerConfirmPassword"/>
                {this.state.error ?<span className="error">{this.state.confirmPwdError}</span>: ''}
              </div>
              <button className="btn-primary btn" type="submit">Submit</button>
              <div className="mt-3">
                <Link to="/login"><span className="login-link">Go to Login?</span></Link>
              </div>
            </form>     
          </div>
        </div>
       </div>
      );
  }
}

const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps)(SignUpComponent);
