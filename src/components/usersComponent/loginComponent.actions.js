export const updateLoginInfo = data => ({
  type: 'setLoginInfo',
  payload: data
});