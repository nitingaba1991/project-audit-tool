// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';
import configObj from '../../config';
import queryString from 'query-string';
import { connect } from 'react-redux';
class AddEditRecordComponent extends Component {
  constructor() {
    super();
    this.state = {
      error: null,
      value: '',
      loading: false,
      isLoaded: false,
      data: {
        project_name: '',
        client_name: '',
        question_title: '',
        category_type: 'general',
        options: '',
        required: 'true'
      }
    };
    this.updateState = this.updateState.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if(!this.props.intialState.user.isLoggedIn || (this.props.intialState.user.isLoggedIn && this.props.intialState.user.role !== 'admin')) {
      this.props.history.push('/');
       toastObj.showMessage(true, 'Please login to view this page')
    }
    else {
      if (this.props.match.params.action === 'edit') {
        const values = queryString.parse(this.props.location.search);
        let url = '/api/getData?_id=' + values._id + '&type=' + values.type;
        this.setState({ loading: true })
        axios.get(url)
        .then((res) => {
          var options = '';
          if(res.data.data[0].options) {
            options = res.data.data[0].options.join();
          }
          res.data.data[0].options = options;
          var data = {};
          if(this.props.match.params.entity == 'questions') {
            data = {
              _id: res.data.data[0]._id,
              category_type:  res.data.data[0].category_type,
              question_title:  res.data.data[0].question_title,
              required:  res.data.data[0].required,
              options: options
            }
          } else {
            data = {
              client_name:  res.data.data[0].client_name,
              _id: res.data.data[0]._id,
              project_name: res.data.data[0].project_name
            }
          }
          this.setState({ loading: false});
          this.setState({data});
       });
      }
    }
  }

  goToBack = (e) => {
    this.props.history.push('/');
  }

  updateState = (e) => {
    var obj = this.state.data;
    obj[e.target.name] = e.target.value;
    this.setState({data: obj
    });
  }
  
  handleSubmit(event) {
    event.preventDefault();
    let url = '';
    if(this.props.match.params.entity === 'projects') {
      url = '/api/addProjectData';
      var data = {
        project_name: this.state.data.project_name,
        client_name: this.state.data.client_name
      };
      this.setState({error: false,projectnameError: '',clientnameError: ''})
      if(data.project_name == '') {
        this.setState({error: true, projectnameError: 'Required Field'})
        return false;
      }
      if(data.client_name == '') {
        this.setState({error: true, clientnameError:'Required Field'});
        return false;
      }
    } else {
      url = '/api/addQuestionData';
      var data = {
        question_title: this.state.data.question_title,
        category_type: this.state.data.category_type,
        required: this.state.data.required
      };
      this.setState({error: false,questionTitleError: ''})
      if(data.question_title == '') {
        this.setState({error: true, questionTitleError: 'Required Field'});
        return false;
      }
    }
    if(this.props.match.params.action === 'edit') {
      data._id = this.state.data._id;
      url = '/api/updateData';
      this.updateData(event,data, url);
    } else {
      this.submitData(event,data, url);
    }
  }
  submitData = (event, data, url) => {
    let props = this.props
    axios.post(url, data)
    .then(function (response) {
       toastObj.showMessage(response.data.error, response.data.message);
       if (url.indexOf('addQuestionData') > -1) {
          props.history.push('/admin/view/questions');
        } else {
          props.history.push('/');
        }
    })
    .catch(function (error) {
       toastObj.showMessage(true, 'Error in adding!');
    });
  };

  updateData = (event, data, url) => {
    let props = this.props;
    if(props.match.params.entity === 'projects') {
      data.type = 'projects';
    } else {
      data.type = 'questions';
    }
    axios.put(url, data)
    .then(function (response) {
       toastObj.showMessage(response.data.error, response.data.message);
       if (data.type === 'questions') {
          props.history.push('/admin/view/questions');
        } else {
          props.history.push('/');
        }
    })
    .catch(function (error) {
       toastObj.showMessage(true, 'Error in Updating!');
    });
  }
  
  render() {
    let projectTemplate = '';
    if (this.props.match.params.entity === 'projects') {
      projectTemplate = <div>
      <h4>Add Project</h4>
      <div className="form-group">
        <label>Project Name<span className="required">*</span></label>
        <input type="text" onChange={this.updateState} value={this.state.data.project_name} className="form-control" id="projectName" name="project_name"/>
        {this.state.error ?<span className="error">{this.state.projectnameError}</span>: ''}
      </div>
      <div className="form-group">
        <label>Client Name<span className="required">*</span></label>
        <input type="text" onChange={this.updateState} value={this.state.data.client_name} className="form-control" id="client_name" name="client_name"/>
        {this.state.error ?<span className="error">{this.state.clientnameError}</span>: ''}
      </div>
      <button className="btn btn-primary" type="submit">Submit</button>
      <button className="btn btn-secondary ml-3" onClick={() => this.goToBack()}>Cancel</button>
    </div>  
    } 
    else if(this.props.match.params.entity === 'questions') {
      projectTemplate = <div>
      <h4>Add Question</h4>
      <div className="form-group">
        <label>Title<span className="required">*</span></label>
        <input type="text" onChange={this.updateState} value={this.state.data.question_title} className="form-control" id="question_title" name="question_title"/>
        {this.state.error ?<span className="error">{this.state.questionTitleError}</span>: ''}
      </div>
      <div className="form-group">
        <label>Required</label>
        <select value={this.state.data.required} onChange={this.updateState} id="required" name="required" className="form-control">
          <option value="true">Yes</option>
          <option value="false" >No</option>
        </select>
        {/* <input type="text" onChange={this.updateState} value={this.state.data.required} className="form-control" id="options" name="required"/> */}
      </div>
      <div className="form-group">
        <label>Question Category type (Used for filter questions on audit page)</label>
        <select value={this.state.data.category_type} onChange={this.updateState} id="category_type" name="category_type" className="form-control">
          {configObj.questionCategoryTab.map(type => (
            <option value={type.id}>{type.name}</option>
        ))}
        </select>
      </div>
      <button className="btn btn-primary" type="submit">Submit</button>
      <button className="btn btn-secondary ml-3" onClick={() => this.goToBack()}>cancel</button>
    </div>
    }
    
    return (
      <div className="container mt-3">
        <div className="row justify-content-center">
          <div className="col-12 col-lg-6">
            <form autoComplete="off" onSubmit={this.handleSubmit}>
              {projectTemplate}
            </form>
          </div>
        </div>
      </div>
      
      )
  }
}

const mapStateToProps = function(state) {
  return {
    intialState: state
  }
};
export default connect(mapStateToProps)(AddEditRecordComponent);
