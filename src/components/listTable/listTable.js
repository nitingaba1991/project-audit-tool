
// /client/App.js
import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from '../Modal/Modal';
import axios from 'axios';
class ListTable extends Component {
  constructor(props) {
    super();
    this.state = {
      showModal: false,
      selectedData: {},
      auditData: {}
    }
    this.deleteData = this.deleteData.bind(this);
    this.getdata = this.getdata.bind(this);
  }
  getDataFromDb = (item) => {
    let url = `/api/getData?project_id=${item._id}&type=audits`;
    axios.get(url)
      .then((res) => {this.setState({ auditData: res.data.data[0] })});
      $('.auditModal').modal('show');
  };
  
  editRecord = (item) => {
    this.props.history.push(`/admin/edit/projects?_id=${item._id}&type=projects`);
  }
  goToAudit = (item) => {
    if(item.auditSubmited) {
      this.getDataFromDb(item);
    } else {
      this.props.history.push(`/audit/?_id=${item._id}&type=projects`);
    }
  }
  
  getdata = () => {
    this.props.getDbData();
  }
  deleteData = (item) => {
    this.setState({selectedData: item})
    $('.delete-modal').modal('show');
  }
 
  render() {
    let questionTemplate = '';
    if(this.state.auditData.questionData && this.state.auditData.questionData.length) {
      questionTemplate = this.state.auditData.questionData.map(questionItem => (
        <div>
          <h6>Question Title: {questionItem.title}</h6>
          <p>Recommendation: {questionItem.answer}</p>
        </div>
       ))
    }
   

    let template = this.props.data.map(item => (
      <tr key={item._id}>
        <td  colSpan="2">{item.project_name}</td>
        <td>{item.client_name}</td>
        <td>{item.pid}</td>
        {this.props.intialState.user.isLoggedIn && this.props.intialState.user.role === 'admin' ? 
        <td><i className="fa mr-2 fa fa-search" onClick={() => this.goToAudit(item)} title="Audit/View Report"></i> <i className="fa mr-2 fa-edit" onClick={() => this.editRecord(item)} title="edit"></i> <i className="fa mr-2 fa-trash" onClick={() => this.deleteData(item)} title="delete"></i> </td>: 
        <td><i className="fa mr-2 fa fa-search" onClick={() => this.goToAudit(item)} title="Audit/View Report"></i> </td>}
        </tr>
    ))
    return (
      <React.Fragment>
      <Modal getdata={this.getdata} id={this.state.selectedData._id} type="projects"/>
    <div className="container">
      <div className="row">
        <div className="col-12">
          <table className="mt-3 project table text-center">
            <thead>
              <tr>
              <th colSpan="2">Name</th>
              <th>Client</th>
              <th>Pid</th>
              <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.props.data && this.props.data.length ? template : <tr><td>No Projects Found. Please add project First.</td></tr>}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div className="modal auditModal" tabIndex="-1" role="dialog">
    <div className="modal-dialog modal-xl modal-dialog-centered" role="document">
      <div className="modal-content">
        <div className="modal-header">
          <h6 className="modal-title">Audit Report submitted already. See the report below</h6>
          <button type="button" className="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          {questionTemplate}
        </div>
      </div>
    </div>
  </div>
  </React.Fragment>
  )

  }
}

// const mapDispatchToProps = dispatch => ({
//   setLogin: (userData) => dispatch(updateLoginInfo(userData))
// });
const mapStateToProps = function(state) {
  return {
    intialState: state
  }
}
export default connect(mapStateToProps)(ListTable);

