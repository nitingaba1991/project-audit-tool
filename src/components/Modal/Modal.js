// /client/App.js
import React, { Component } from 'react';
import axios from 'axios';

class Modal extends Component {
  constructor(props) {
    super();
    this.state = {
      auditData: {}
    }
    this.deleteData = this.deleteData.bind(this);
  }

  deleteData = () => {
    let objData = {
      id: this.props.id,
      type: this.props.type
    }
    axios.delete('/api/deleteData', {data: objData})
      .then((response) => {
        if(response.data.error){
          toastObj.showMessage(response.data.error, response.data.message);
        }
        else {
          toastObj.showMessage(response.data.error, response.data.message);
          this.props.getdata();
          $('.delete-modal').modal('hide')
        }
    });
  };
  render() {
    return (
      <div className="modal delete-modal" tabIndex="-1" role="dialog">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-body">
            <p>Are you sure you want to delete?</p>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" className="btn btn-primary" onClick={this.deleteData}>Delete</button>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

export default Modal;