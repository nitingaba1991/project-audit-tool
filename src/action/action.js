
export const add = () => ({
    type: 'ADD',
    payload: { value: 12 },
});

/**
 * Action Creator. Returns an action of the type 'SUBTRACT'
 */
export const subtract = () => ({
    type: 'SUBTRACT',
    payload: { value: 12 },
});

/**
 * Action Creator. Returns an action of the type 'RESET'
 */
export const reset = () => ({ type: 'RESET' });