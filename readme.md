# Install node
# install mongodb
# goto audit tool directory
# run command npm install
# After installing node modules run command npm run prod or npm run dev
# Open localhost:4000 in browser

# Funtional Doc
# User can register and login on the website 
# User can have two roles 1.) admin 2.) member user
# Admin user can add/edit/delete projects and questions available in system also can audit the project
# Normal user wont be able to use above features can only audit the projects.
# To register as a admin user I have defined admin email ids in config.js under backend folder. i.e appConfig.adminUsers = ['admin@gmail.com', 'admin1@gmail.com', 'admin2@gmail.com', 'admin3@gmail.com', 'admin4@gmail.com']; if a user is registered using these email address he/she will registered as a admin user else the role of the user will be member
# While adding questions one can select the type. Use of this field is that question will see under that tab while auditing
# After audit the project one can view report on clicking the same icon i.e. search icon on home page.

# Next steps
# Improve Validation part on Frontend
# We can give option to admin user to add new admin/member users
# Not able to use material design and graphQl due to time constraint
# Can send email address on submition on audit
# Not able to give functionality to forgot password or edit profile/ change profile